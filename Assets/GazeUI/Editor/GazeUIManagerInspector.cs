﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GazeUIManager))]
public class GazeUIManagerEditor : Editor {

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (GUILayout.Button("Edit Gaze UI Settings"))
        {
            Selection.activeObject = ((GazeUIManager)target).settings;
        }
    }
}
