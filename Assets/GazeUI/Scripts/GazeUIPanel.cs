﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System;

[RequireComponent(typeof(CanvasGroup))]
public class GazeUIPanel : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    public CanvasGroup canvasGroup;
    public bool activateOnMouseEnter = true;
    public bool deactivateOnMouseExit = true;

    private bool isActive = false;
    private Coroutine gazeCoroutine;

    void Awake()
    {
        HidePanel();
    }

    public void ActivatePanel()
    {
        isActive = true;

        foreach (Transform t in transform)
            t.gameObject.SetActive(true);

        CancelInvoke("DeactivatingPanel");

        if (gazeCoroutine == null)
            gazeCoroutine = StartCoroutine(Gazing());
    }

    public void DeactivatePanel()
    {
        Invoke("DeactivatingPanel", GazeUISettings.instance.panelStayAliveAfterGazeExit);
    }
    
    void DeactivatingPanel()
    {
        isActive = false;
    }

    IEnumerator Gazing()
    {
        do
        {
            if (isActive)
            {
                canvasGroup.alpha += GazeUISettings.instance.panelFadeInSpeed * Time.deltaTime;
            }
            else
            {
                canvasGroup.alpha -= GazeUISettings.instance.panelFadeOutSpeed * Time.deltaTime;
            }
            canvasGroup.alpha = Mathf.Clamp01(canvasGroup.alpha);

            yield return new WaitForEndOfFrame();
        }
        while (canvasGroup.alpha > 0f);

        gazeCoroutine = null;
        HidePanel();

        yield break;
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        //if isActive == true than just cancel invoked deactivation
        if (activateOnMouseEnter || isActive)
            ActivatePanel();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (deactivateOnMouseExit)
            DeactivatePanel();
    }

    void OnDisable()
    {
        HidePanel();
    }

    void HidePanel()
    {
        isActive = false;
        canvasGroup.alpha = 0f;
        foreach (Transform t in transform)
            t.gameObject.SetActive(false);

        if(gazeCoroutine != null)
        {
            StopCoroutine(gazeCoroutine);
            gazeCoroutine = null;
        }
    }
}
