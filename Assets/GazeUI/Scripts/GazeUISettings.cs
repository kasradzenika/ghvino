﻿using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class GazeUISettings : ScriptableObject
{
    //it's the responsibility of the developer NOT to keep 2 instances of this asset in the project
    public static GazeUISettings instance;

    [Header("Button Settings")]
    public float gazeProgressTime = 1f;
    public float gazeGainFocusSpeed = 1f;
    public float gazeLoseFocusSpeed = 2f;
    public float gazeStayAfterClickTime = 0.3f;

    [Header("Panel Settings")]
    public float panelFadeInSpeed = 1f;
    public float panelFadeOutSpeed = 1f;
    public float panelStayAliveAfterGazeExit = 1f;

    void Awake()
    {
        AlterScriptExecutionOrder();
    }

    public void LoadSettings()
    {
        instance = this;
    }

    void AlterScriptExecutionOrder()
    {
#if UNITY_EDITOR
        // Get the name of the script we want to change it's execution order
        string scriptName = typeof(GazeUIManager).Name;

        // Iterate through all scripts (Might be a better way to do this?)
        foreach (MonoScript monoScript in MonoImporter.GetAllRuntimeMonoScripts())
        {
            // If found our script
            if (monoScript.name == scriptName)
            {
                // And it's not at the execution time we want already
                // (Without this we will get stuck in an infinite loop)
                if (MonoImporter.GetExecutionOrder(monoScript) != -1005)
                {
                    MonoImporter.SetExecutionOrder(monoScript, -1005);
                }
                break;
            }
        }
#endif
    }
}