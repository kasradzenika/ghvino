﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using System.Collections;

public class GazeUIButton : MonoBehaviour,
    IPointerEnterHandler,
    IPointerExitHandler,
    IPointerClickHandler    //without pointer click handler the gvr reticle is not updating
{
    public UnityEngine.UI.Image progressBar;
    public UnityEvent OnTriggerGaze;
    
    private Coroutine gazeCoroutine;
    private bool didTrigger = false;
    private bool didClickBeforeTrigger = false;
    private bool isGazing = false;

    IEnumerator Gazing()
    {
        float gazeProgress = 0f;

        do
        {
            yield return new WaitForEndOfFrame();
            if (didClickBeforeTrigger)
                gazeProgress = GazeUISettings.instance.gazeProgressTime;
            else if(isGazing)
                gazeProgress += Time.deltaTime * GazeUISettings.instance.gazeGainFocusSpeed;
            else
                gazeProgress -= Time.deltaTime * GazeUISettings.instance.gazeLoseFocusSpeed;

            gazeProgress = Mathf.Clamp(gazeProgress, 0f, GazeUISettings.instance.gazeProgressTime);

            if (progressBar != null)
                progressBar.fillAmount = gazeProgress / GazeUISettings.instance.gazeProgressTime;

        }
        while (gazeProgress > 0f && gazeProgress < GazeUISettings.instance.gazeProgressTime);

        //if did not click and progress is full trigger gaze
        if(!didClickBeforeTrigger && gazeProgress >= GazeUISettings.instance.gazeProgressTime)
            TriggerGaze();

        //reset
        didClickBeforeTrigger = false;
        gazeCoroutine = null;

        if(didClickBeforeTrigger)
            progressBar.fillAmount = 0f;

        yield break;
    }

    public void TriggerGaze()
    {
        didTrigger = true;
        OnTriggerGaze.Invoke();
    }


    //Interfaces beyond this point

    public void OnPointerEnter(PointerEventData eventData)
    {
        isGazing = true;
        if (gazeCoroutine == null)
            gazeCoroutine = StartCoroutine(Gazing());
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        isGazing = false;

        if (didTrigger)
            progressBar.fillAmount = 0f;

        didTrigger = false;
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        didClickBeforeTrigger = true;
        TriggerGaze();
    }

    void OnDisable()
    {
        progressBar.fillAmount = 0f;
    }
}
