﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;


public class MultiMediaController : MonoBehaviour {
	
	public MediaPlayerCtrl mediaPlayer;

    public float transitionTime = 0.5f;
	private float transitionTimer = 0f;

	public UnityEngine.UI.Image fader;
	public GameObject videoSetCanvas;
	public GameObject infoSetCanvas;

	public AudioSource musicPlayer;
	public GameObject loadingIndicator;
	public Transform sphere360;
	public bool useOnline = true;
	public VideoSet[] videoSets;
	public VideoSet[] infoSets;
    
	private int currentVideoSetIndex = 0;
	private int currentInfoSetIndex = 0;
	private bool isInfoSet = false;
	private bool newVideoIsLoaded = false;

	public static MultiMediaController instance;

	public VideoSet CurrentSet {
		get{
			if(isInfoSet){
				return infoSets[currentInfoSetIndex];
			}
			else{
				return videoSets[currentVideoSetIndex];
			}
		}
	}

	void Start () {
		instance = this;

		loadingIndicator.SetActive(true);
		StartCoroutine(WaitingForVideo());

		currentVideoSetIndex = 0;
		currentInfoSetIndex = 0;
		CurrentSet.ActivateSet();	//the first set is activated by default

		mediaPlayer.Load(CurrentSet.VideoURL);

		mediaPlayer.OnReady = OnVideoReady;
        //ApplyVRSettings();
    }

	public void LoadSet(int index){
		currentVideoSetIndex = index;
		isInfoSet = false;

		StartCoroutine(StartFadeOut());
	}

	public void LoadInfoSet(int index){
		currentInfoSetIndex = index;
		isInfoSet = true;

		StartCoroutine(StartFadeOut());
	}

	public void UnloadInfoSet(){
		LoadSet(currentVideoSetIndex);
	}

	void OnVideoReady(){
		newVideoIsLoaded = true;
	}


    IEnumerator StartFadeOut(){
		loadingIndicator.SetActive(true);

		float ratio;
		transitionTimer = 0f;
		fader.gameObject.SetActive(true);
        
		mediaPlayer.Load(CurrentSet.VideoURL);

		while(transitionTimer < transitionTime){
			ratio = (transitionTimer / transitionTime);
			AudioListener.volume = ratio;
			fader.color = Color.clear + Color.black * ratio;
			transitionTimer += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}

		StartCoroutine(WaitingForVideo());
	}

	IEnumerator WaitingForVideo(){
		AudioListener.volume = 0f;
		fader.color = Color.black;

		for (int i = 0; i < videoSets.Length; i++) {
			videoSets[i].DeactivateSet();
		}

		sphere360.rotation = Quaternion.Euler(0f, CurrentSet.pinSetYRotation, 0f);

		if(Application.platform != RuntimePlatform.OSXEditor && Application.platform != RuntimePlatform.WindowsEditor){
			while(!newVideoIsLoaded){
				yield return new WaitForEndOfFrame();
			}
		}

		loadingIndicator.SetActive(false);
		CurrentSet.ActivateSet();

		newVideoIsLoaded = false;

		StartCoroutine(StartFadeIn());
	}

	IEnumerator StartFadeIn(){
		if(CurrentSet.lowerMusic){
			musicPlayer.volume = 0.1f;
		}
		else {
			musicPlayer.volume = 1f;
		}

		float ratio;
		transitionTimer = 0f;
		while(transitionTimer < transitionTime){
			ratio = (1 - transitionTimer / transitionTime);
			AudioListener.volume = ratio;
			fader.color = Color.clear + Color.black * ratio;
			transitionTimer += Time.deltaTime;
			yield return new WaitForEndOfFrame();
		}

		fader.gameObject.SetActive(false);

		infoSetCanvas.SetActive(isInfoSet);
		videoSetCanvas.SetActive(!isInfoSet);

		AudioListener.volume = 1f;
	}
}
