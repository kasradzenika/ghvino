﻿using UnityEngine;
using UnityEngine.EventSystems;

public enum PinState { Passive = 0, Active, DidStartAction }

public class PinController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler {
	
	public Color passiveColor;
	public Color activeColor;
	public Color didStartActionColor;
	public Color visitedColor;
	public int transitionTo = 0;
	public float transitionSpeed = 5f;

	private PinState state;
	private Material mat;
	private ParticleSystem didStartActionParticles;

	private bool thisSetWasAlreadyVisited = false;
    private float gazeStartTime;

	void Start () {
		mat = transform.GetComponentInChildren<MeshRenderer>().material;
		didStartActionParticles = transform.GetComponentInChildren<ParticleSystem>();
	}

	void OnEnable() {
		state = PinState.Passive;
		if(MultiMediaController.instance.videoSets[transitionTo].gotVisited){
			thisSetWasAlreadyVisited = true;
		}

		Debug.Log("thisSetWasAlreadyVisited " + transitionTo + ": " + thisSetWasAlreadyVisited);
	}

	void Update () {
		AdjustPin();
	}

	void AdjustPin(){
		switch(state){
		    case PinState.Passive:

			    if(thisSetWasAlreadyVisited){
				    mat.color = Color.Lerp(mat.color, visitedColor, transitionSpeed * Time.deltaTime);
			    }
			    else{
				    mat.color = Color.Lerp(mat.color, passiveColor, transitionSpeed * Time.deltaTime);
			    }
			    break;
		    case PinState.Active:
			    mat.color = Color.Lerp(mat.color, activeColor, transitionSpeed * Time.deltaTime);
                //manually do gaze
                if(Time.timeSinceLevelLoad - gazeStartTime > GazeUISettings.instance.gazeProgressTime)
                {
                    ActivatePin();
                }
			    break;
		    case PinState.DidStartAction:
			    mat.color = Color.Lerp(mat.color, didStartActionColor, transitionSpeed * Time.deltaTime);
			    break;
		}
	}

	public void OnPointerEnter(PointerEventData data){
		SFX.instance.PlayPinGazeAudio();
        gazeStartTime = Time.timeSinceLevelLoad;
        state = PinState.Active;
	}

	public void OnPointerExit(PointerEventData data){
		SFX.instance.PausePinGazeAudio();

		if(state != PinState.DidStartAction)
			state = PinState.Passive;
	}

	public void OnPointerClick(PointerEventData data){
        ActivatePin();
	}

    void ActivatePin()
    {
        didStartActionParticles.Play();
        SFX.instance.PlayTransitionAudio();
        state = PinState.DidStartAction;

        MultiMediaController.instance.LoadSet(transitionTo);
    }
}
