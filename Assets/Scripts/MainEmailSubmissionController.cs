﻿using UnityEngine;
using UnityEngine.UI;

public class MainEmailSubmissionController : MonoBehaviour {
    
    public string enterMailString;
    public string thankYouString;
    public string enterValidMailString;
    public string submittingEmail;
    public string noInternetString;

    public Text enterMailText;
    public InputField emailInput;
    public Transform submitMailButton;
    public Transform submitMailBackButton;

    public MainStateManager mainStateManager;

    public void OnClickSubmitMail()
    {
        enterMailText.text = submittingEmail;
        emailInput.interactable = false;
        submitMailButton.GetComponent<Button>().interactable = false;
        submitMailBackButton.GetComponent<Button>().interactable = false;

        AWSHandler.instance.SubmitEmails(new string[] { emailInput.text }, SubmitMailCallback);
    }

    void SubmitMailCallback(AWSHandler.AWSHandlerCallbackCode code, string message)
    {
        if (code == AWSHandler.AWSHandlerCallbackCode.Success)
        {
            mainStateManager.State = MainState.DidEnterEmail;

            //textUI.gameObject.SetActive(true);
            //textUI.text = thankYouString;
            //mailForm.gameObject.SetActive(false);
        }
        else if (code == AWSHandler.AWSHandlerCallbackCode.ErrorInvalidEmail)
        {
            enterMailText.text = enterValidMailString;
        }
        else if (code == AWSHandler.AWSHandlerCallbackCode.ErrorNoInternet)
        {
            mainStateManager.State = MainState.DidEnterEmail;

            //textUI.gameObject.SetActive(true);
            //textUI.text = noInternetString;
            //mailForm.gameObject.SetActive(false);
        }
        else if (code == AWSHandler.AWSHandlerCallbackCode.ErrorTimedOut)
        {
            enterMailText.text = message;
        }
        else if (code == AWSHandler.AWSHandlerCallbackCode.ErrorAWSRejected)
        {
            enterMailText.text = message;
        }
        else
        {
            Debug.LogError("Unrecoginzed response code!");
        }

        emailInput.interactable = true;
        submitMailButton.GetComponent<Button>().interactable = true;
        submitMailBackButton.GetComponent<Button>().interactable = true;
    }
}
