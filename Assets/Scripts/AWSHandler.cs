﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Networking;
using System.Collections;
using System.Collections.Generic;
//using System.Net.Mail;
using System.Text.RegularExpressions;
using System;

public class AWSHandler : MonoBehaviour {

    public string serverUrl;
    public float emailSubmittingTimeout = 3f;
    public AWSHandlerStateEvent submitPendingEmailsEvent;

    private AWSHandlerState _state;
    public AWSHandlerState State
    {
        get
        {
            return _state;
        }
        private set
        {
            _state = value;
            submitPendingEmailsEvent.Invoke(_state);
        }
    }

    private UnityAction<AWSHandlerCallbackCode, string> submitEmailCallback;

    private bool _internetAvailable;
    public bool InternetAvailable
    {
        get
        {
            return _internetAvailable;
        }
        private set
        {
            DidConnectToInternet();
            _internetAvailable = value;
        }
    }

    public static AWSHandler instance;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);

        submitPendingEmailsEvent = new AWSHandlerStateEvent();
        StartCoroutine(InternetConnectionWatcher());
    }

    public void SubmitEmails(string[] emails, UnityAction<AWSHandlerCallbackCode, string> callback = null)
    {
        if(State != AWSHandlerState.SubmittingPendingEmails)
            State = AWSHandlerState.SubmittingNewEmails;

        if (callback != null)
            submitEmailCallback = callback;
        else
            submitEmailCallback = (AWSHandlerCallbackCode code, string message) => { }; //too lazy to check if callback is empty all the time, so just use this empty callback

        StartCoroutine(SubmitEmailCoroutine(emails));
    }

    float emailSubmittingStartTime = 0f;
    IEnumerator SubmitEmailCoroutine(string[] emails)
    {
        if (!ValidateEmail(emails))
        {
            submitEmailCallback.Invoke(
                AWSHandlerCallbackCode.ErrorInvalidEmail,
                "Please enter a valid email address!");
            State = AWSHandlerState.Ready;
            yield break;
        }

        //send request
        AWSEmailPostData postData = emails;
        Debug.Log(JsonUtility.ToJson(postData));
        byte[] postDataBytes = System.Text.Encoding.ASCII.GetBytes(JsonUtility.ToJson(postData));

        UploadHandler uploadHandler = new UploadHandlerRaw(postDataBytes);
        DownloadHandlerBuffer downloadHandler = new DownloadHandlerBuffer();
        UnityWebRequest request = new UnityWebRequest(serverUrl, UnityWebRequest.kHttpVerbPOST, downloadHandler, uploadHandler);

        if (Application.internetReachability != NetworkReachability.NotReachable)
        {
            request.Send();
        }
        else
        {
            submitEmailCallback.Invoke(AWSHandlerCallbackCode.ErrorNoInternet, "");
            AWSEmailPostData.AddToCachedEmails(emails);
            yield break;
        }

        emailSubmittingStartTime = Time.timeSinceLevelLoad;

        //progress
        while (!request.isDone && Time.timeSinceLevelLoad < emailSubmittingStartTime + emailSubmittingTimeout)
            yield return new WaitForSeconds(0.1f);

        if (!request.isDone)
        {
            submitEmailCallback.Invoke(AWSHandlerCallbackCode.ErrorNoInternet, "");
            AWSEmailPostData.AddToCachedEmails(emails);
        }
        else if (request.error != null)
        {
            submitEmailCallback.Invoke(AWSHandlerCallbackCode.ErrorAWSRejected,
                string.Format("Error: {0}", request.error));
            AWSEmailPostData.AddToCachedEmails(emails);
        }
        else if (Time.timeSinceLevelLoad >= emailSubmittingStartTime + emailSubmittingTimeout)
        {
            submitEmailCallback.Invoke(AWSHandlerCallbackCode.ErrorTimedOut,
                string.Format("Error: request timed out"));
            AWSEmailPostData.AddToCachedEmails(emails);
        }
        else
        {
            #region debug stuff
            //Debug.Log(request.responseCode);

            //foreach (KeyValuePair<string, string> header in request.GetResponseHeaders())
            //    Debug.Log(string.Format("{0} : {1}", header.Key, header.Value));
            #endregion

            bool amazonServerSideError = false;
            foreach (KeyValuePair<string, string> header in request.GetResponseHeaders())
            {
                if (header.Key.Contains("Error") || header.Key.Contains("error"))
                {
                    amazonServerSideError = true;
                    AWSEmailPostData.AddToCachedEmails(emails);
                }
            }

            //add process rejected emails back to cache
            //TODO: also add the reason of rejection to each email
            AWSEmailPostData responseData = JsonUtility.FromJson<AWSEmailPostData>(downloadHandler.text);
            AWSEmailPostData.AddToCachedEmails(responseData.emails);

            if (!amazonServerSideError)
            {
                submitEmailCallback.Invoke(AWSHandlerCallbackCode.Success, "Success");
            }
        }

        State = AWSHandlerState.Ready;

        yield break;
    }

    public void ResubmitStoredEmails()
    {
        AWSEmailPostData awsEmails = AWSEmailPostData.GetCachedEmails();
        AWSEmailPostData.ClearCachedEmails();

        State = AWSHandlerState.SubmittingPendingEmails;
        SubmitEmails(awsEmails.emails.ToArray());
    }

    void DidConnectToInternet()
    {
        Debug.Log("DidConnectToInternet");
        ResubmitStoredEmails();
    }

    IEnumerator InternetConnectionWatcher()
    {
        yield return new WaitForSeconds(1f);

        while (true)
        {
            if (InternetAvailable)
            {
                if (Application.internetReachability == NetworkReachability.NotReachable)
                {
                    InternetAvailable = false;  //got disconnected
                }
                yield return new WaitForSeconds(30f);   //check less often
            }
            else
            {
                if (Application.internetReachability != NetworkReachability.NotReachable)
                {
                    InternetAvailable = true;  //got connected
                }
                yield return new WaitForSeconds(10f);   //check more often
            }
        }
    }

    #region EmailValidation
    bool EmailIsValid(string emailAddress)
    {
        string validEmailPattern = string.Format("{0}{1}{2}",
            @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|",
            @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)",
            @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$");

        return new Regex(validEmailPattern, RegexOptions.IgnoreCase).IsMatch(emailAddress);
    }

    bool ValidateEmail(string[] emailAddresses)
    {
        if (emailAddresses == null || emailAddresses.Length == 0)
            return false;

        foreach (string emailAddress in emailAddresses)
            if (!EmailIsValid(emailAddress))
                return false;

        return true;
    }
    #endregion

    public enum AWSHandlerState
    {
        Ready,
        SubmittingNewEmails,
        SubmittingPendingEmails
    }

    public class AWSHandlerStateEvent : UnityEvent<AWSHandlerState> { }

    public enum AWSHandlerCallbackCode
    {
        Success = 0,
        ErrorTimedOut,
        ErrorNoInternet,
        ErrorInvalidEmail,
        ErrorAWSRejected
    }

}

public class AWSEmailPostData
{
    public List<string> emails;
    public static string StoredEmailsPref = "StoredEmails";

    #region caching system
    public static AWSEmailPostData GetCachedEmails()
    {
        AWSEmailPostData cachedEmails = JsonUtility.FromJson<AWSEmailPostData>(PlayerPrefs.GetString(StoredEmailsPref, "{}"));
        return cachedEmails;
    }

    public static void AddToCachedEmails(string email)
    {
        AddToCachedEmails(new string[] { email });
    }

    public static void AddToCachedEmails(List<string> emails)
    {
        AddToCachedEmails(emails.ToArray());
    }

    public static void AddToCachedEmails(string[] emails)
    {
        AWSEmailPostData cachedEmails = JsonUtility.FromJson<AWSEmailPostData>(PlayerPrefs.GetString(StoredEmailsPref, "{}"));
        cachedEmails.emails.AddRange(emails);
        PlayerPrefs.SetString(StoredEmailsPref, JsonUtility.ToJson(cachedEmails));
    }

    public static void ClearCachedEmails()
    {
        PlayerPrefs.DeleteKey(StoredEmailsPref);
    }
    #endregion

    #region implicit convertors
    public static implicit operator AWSEmailPostData(string[] _emails)
    {
        AWSEmailPostData m = new AWSEmailPostData();
        m.emails = new List<string>(_emails);
        return m;
    }

    public static implicit operator AWSEmailPostData(List<string> _emails)
    {
        AWSEmailPostData m = new AWSEmailPostData();
        m.emails = _emails;
        return m;
    }
    #endregion
}