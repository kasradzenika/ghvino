﻿using UnityEngine;

[System.Serializable]
public class VideoSet
{
	public string videoName;
	public int videoIndex;

	public GameObject pinSetContainer;
	public float pinSetYRotation;

	public string onlineVideoURL;
	public string offlineVideoURL;
	public string editorVideoURL;

	//[HideInInspector]
	public bool gotVisited;

	public bool lowerMusic;

	private bool didCheckFileName;
	private bool cached;

    public string VideoURL {
        get {
            if(MultiMediaController.instance.useOnline){
				return onlineVideoURL;
			}
			//in editor use proxy paths
			else if(Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor){

				//rename file if it is directly downloaded from s3
				if(!didCheckFileName && !System.IO.File.Exists(editorVideoURL) && System.IO.File.Exists(editorVideoURL.Replace(' ', '+'))){
					System.IO.File.Move(editorVideoURL.Replace(' ', '+'), editorVideoURL);
				}

				didCheckFileName = true;

				return editorVideoURL;
			}
			else{

				//rename file if it is directly downloaded from s3
				if(!didCheckFileName && !System.IO.File.Exists(offlineVideoURL) && System.IO.File.Exists(offlineVideoURL.Replace(' ', '+'))){
					System.IO.File.Move(offlineVideoURL.Replace(' ', '+'), offlineVideoURL);
				}

				didCheckFileName = true;

				//file.exists takes some time to check, if cached is true than this check is avoided
				if(cached || System.IO.File.Exists(offlineVideoURL)){
					cached = true;
					return "file://" + offlineVideoURL;
				}
				//useOnline is false but video not cached
				else {
					cached = false;
					return onlineVideoURL;
				}
			}
		}
	}

	public VideoSet(string name, int index, GameObject pinSet, float yRotation, string onlineURL, string offlineUrl, string editorURL, bool lowerOtherSounds = false)
	{
		videoName = name;
		videoIndex = index;

		pinSetContainer = pinSet;
		pinSetYRotation = yRotation;

		gotVisited = false;
		didCheckFileName = false;
		cached = false;
		lowerMusic = lowerOtherSounds;

		onlineVideoURL = onlineURL;
		offlineVideoURL = offlineUrl;
		editorVideoURL = editorURL;
	}

	public void ActivateSet(){
		gotVisited = true;

		if(pinSetContainer != null)
			pinSetContainer.SetActive(true);
	}

	public void DeactivateSet(){
		if(pinSetContainer != null)
			pinSetContainer.SetActive(false);
	}
}
