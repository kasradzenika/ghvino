﻿using UnityEngine;
using System.Collections;

public class MainStateManager : MonoBehaviour {
    
    [SerializeField]    //I just want to keep an eye on it in inspector
    private MainState state;

    public MainState State
    {
        get
        {
            return state;
        }
        set
        {
            MainState oldState = state;
            state = value;
            OnStateChanged(oldState, state);
        }
    }

    public MainStateObjects[] mainStateObjects;
	
    void OnStateChanged(MainState fromState, MainState toState)
    {
        DeactivateState(fromState);
        ActivateState(toState);
    }

    void DeactivateState(MainState _state)
    {
        mainStateObjects[(int)_state].Deactivate();
    }

    void ActivateState(MainState _state)
    {
        mainStateObjects[(int)_state].Activate();
    }

    #region setters and getters
    public void SetState(MainState _state)
    {
        State = _state;
    }

    public MainState GetState()
    {
        return State;
    }
    #endregion
}

public enum MainState
{
    Playing = 0,
    RemoveHeadset,
    EnteringEmail,
    DidEnterEmail
}

[System.Serializable]
public class MainStateObjects
{
    public string name; //just for visualization in inspector
    public MainStateToggleEvent StateChanged;
    [Tooltip("Enable/Disable objects")]
    public GameObject[] stateObjects;
    [Tooltip("Inversely Enable/Disable objects as the state")]
    public GameObject[] nonStateObjects;

    public void Activate()
    {
        foreach(GameObject g in stateObjects)
            g.SetActive(true);

        foreach (GameObject g in nonStateObjects)
            g.SetActive(false);

        StateChanged.Invoke(true);
    }

    public void Deactivate()
    {
        foreach (GameObject g in stateObjects)
            g.SetActive(false);

        foreach (GameObject g in nonStateObjects)
            g.SetActive(true);

        StateChanged.Invoke(false);
    }
}

[System.Serializable]
public class MainStateToggleEvent : UnityEngine.Events.UnityEvent<bool> { }
