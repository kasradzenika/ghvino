﻿using UnityEngine;

//controls bottom canvas + onclick callbacks

public class DynamicCanvasController : MonoBehaviour {

	public Transform target;
	public float thresholdXRotation = 5f;

	private Vector3 eulers;

	void Start () 
	{
		eulers = Vector3.zero;

	}

	void Update ()
	{
		if(target.eulerAngles.x < thresholdXRotation)
		{
			eulers.y = target.eulerAngles.y;

			transform.eulerAngles = eulers;
		}
	}
}
