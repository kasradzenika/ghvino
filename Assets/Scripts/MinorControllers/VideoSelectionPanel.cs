﻿using UnityEngine;
using System.Collections;

public class VideoSelectionPanel : MonoBehaviour {
    
    public MultiMediaController multiMediaController;
    public Transform videoButtonsContainer;
    public GameObject videoButton;

	void Start () {
        for (int i = 0; i < multiMediaController.videoSets.Length; i++)
        {
            GameObject bGameObject = (GameObject)Instantiate(videoButton, videoButtonsContainer.position, videoButtonsContainer.rotation);
            bGameObject.transform.SetParent(videoButtonsContainer);

            int videoIndex = i;
            bGameObject.GetComponent<GazeUIButton>()
                .OnTriggerGaze
                .AddListener(() => multiMediaController.LoadSet(videoIndex));

            bGameObject.GetComponentInChildren<UnityEngine.UI.Text>().text = i.ToString();
        }
	}
}
