﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System;
using System.IO;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;
using UnityEngine.Networking;

public class CacheHandler : MonoBehaviour {

    public float connectionTimeOut = 10f;
    
    private DownloadHandlerBuffer downloadHandler;
    private UnityWebRequest downloadRequest;
    private Coroutine downloadCoroutine;
    private UnityAction<string> downloadSuccessfulCallback;
    private int downloadDataCurrentByteLength = 0;
    private int downloadDataTotalByteLength = 0;
    private int downloadDataTotalMegabyteLength = 0;
    private const int MEGABYTE = 1048576;   //2^20 bytes
    private bool downloadIsPaused = false;

    public static CacheHandler instance;

    void Awake()
    {
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        instance = this;
        DontDestroyOnLoad(gameObject);
        FirstTimeSetup();
    }

    public static string OnlineUrlToCachePath(string onlineUrl)
    {
        Uri uri = new Uri(onlineUrl);
        return string.Format("{0}{1}", Application.persistentDataPath, uri.AbsolutePath);
    }
    
    public bool IsOnlineFileCached(string onlineUrl)
    {
        return CacheFileExists(OnlineUrlToCachePath(onlineUrl));
    }

    public bool CacheFileExists(string cachePath)
    {
        if (File.Exists(cachePath))
            return true;

        return false;
    }

    public void DownloadFile(string onlineUrl, UnityAction<string> callback, int fromByte = 0)
    {
        downloadSuccessfulCallback = callback;
        downloadCoroutine = StartCoroutine(DownloadCoroutine(onlineUrl, fromByte));
    }

    public void ClearCachedFileFromUrl(string onlineUrl)
    {
        string localPath = OnlineUrlToCachePath(onlineUrl);
        ClearCachedFile(localPath);
    }

    public int CachedOnlineFileSize(string onlineUrl)
    {
        string localPath = OnlineUrlToCachePath(onlineUrl);
        if (File.Exists(localPath))
        {
            FileInfo info = new FileInfo(localPath);
            return (int)info.Length;    //DANGER TODO make everything work in long-s, not int-s
        }
        return 0;
    }

    public void ClearCachedFile(string cachePath)
    {
        if (File.Exists(cachePath))
            File.Delete(cachePath);
    }

    public float GetDownloadProgress()
    {
        if (downloadRequest != null && downloadDataTotalByteLength > 0)
        {
            return (float)(downloadHandler.data.Length + downloadDataTotalByteLength - downloadDataCurrentByteLength) / downloadDataTotalByteLength;
        }

        return 0f;
    }

    public int GetDownloadSize(bool inMegabytes)
    {
        if (inMegabytes)
            return downloadDataTotalMegabyteLength;
        else
            return downloadDataTotalByteLength;
    }

    public void PauseCurrentDownload()
    {
        downloadIsPaused = true;
    }

    void FirstTimeSetup()
    {
        ServicePointManager.ServerCertificateValidationCallback = MyRemoteCertificateValidationCallback;
    }

    IEnumerator DownloadCoroutine(string onlineUrl, int fromByte)
    {
        //prepare directories
        Uri uri = new Uri(onlineUrl);

        string localPath = Application.persistentDataPath;
        for (int i = 0; i < uri.Segments.Length - 1; i++)
        {
            localPath = string.Format("{0}{1}", localPath, uri.Segments[i]);
        }

        Debug.Log("Checking/Creating dir: " + localPath);

        if(!Directory.Exists(localPath))
            Directory.CreateDirectory(localPath);

        //prepare file path including file name
        localPath = string.Format("{0}{1}", localPath, uri.Segments[uri.Segments.Length - 1]);

        Debug.Log("Creating file: " + localPath);

        //download
        downloadHandler = new DownloadHandlerBuffer();

        downloadRequest = new UnityWebRequest(onlineUrl, UnityWebRequest.kHttpVerbGET, downloadHandler, null);
        downloadRequest.disposeDownloadHandlerOnDispose = true;
        downloadRequest.disposeUploadHandlerOnDispose = true;

        //resume download
        if (fromByte > 0)
            downloadRequest.SetRequestHeader("Range", string.Format("bytes={0}-", fromByte));

        Debug.Log("Sending request");

        float initiationTime = Time.realtimeSinceStartup;
        downloadRequest.Send();

        //waiting for the response
        while (downloadRequest.error == null &&
            downloadRequest.GetResponseHeader("Content-Length") == null &&
            Time.realtimeSinceStartup - initiationTime < connectionTimeOut)
        {
            yield return new WaitForEndOfFrame();
        }

        //error or no response
        if (downloadRequest.error != null ||
            downloadRequest.responseCode > 299 ||
            Time.realtimeSinceStartup - initiationTime >= connectionTimeOut)
        {
            Debug.Log("Aborting connection, something went wrong. Error: " + downloadRequest.error);
            downloadRequest.Dispose();
            yield break;
        }

        Debug.Log("Proceeding with the request...");

        //start writing to disk
        int dataOffset = 0;
        int downloadedChunkSize = 0;

        downloadDataCurrentByteLength = int.Parse(downloadRequest.GetResponseHeader("Content-Length"));

        downloadDataTotalByteLength = CachedOnlineFileSize(onlineUrl) + downloadDataCurrentByteLength;
        PlayerPrefs.SetInt("DownloadSize", downloadDataTotalByteLength);

        downloadDataTotalMegabyteLength = downloadDataTotalByteLength / MEGABYTE;

        FileStream stream = new FileStream(localPath, FileMode.OpenOrCreate);

        while (downloadRequest.error == null &&
            dataOffset < downloadDataCurrentByteLength && !downloadIsPaused)
        {
            if (downloadHandler.data.Length > dataOffset)
            {
                downloadedChunkSize = downloadHandler.data.Length - dataOffset;

                if (downloadedChunkSize > 2)
                {
                    stream.Write(downloadHandler.data, dataOffset, downloadedChunkSize);
                    dataOffset += downloadedChunkSize;
                    Debug.Log("From byte: " + fromByte + " Writing: " + dataOffset);
                }
            }

            yield return new WaitForEndOfFrame();
        }

        if (downloadRequest.error != null)
            Debug.Log(downloadRequest.error);
        
        stream.Close();

        Debug.Log("Donwloaded bytes: " + downloadHandler.data.Length + " data offset: " + dataOffset);

        if (downloadSuccessfulCallback != null)
        {
            downloadSuccessfulCallback.Invoke(localPath);
        }

        downloadDataTotalMegabyteLength = 0;
        downloadRequest.Dispose();

        downloadIsPaused = false;
        yield break;
    }

    bool MyRemoteCertificateValidationCallback(System.Object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
    {
        bool isOk = true;
        // If there are errors in the certificate chain, look at each error to determine the cause.
        if (sslPolicyErrors != SslPolicyErrors.None)
        {
            for (int i = 0; i < chain.ChainStatus.Length; i++)
            {
                if (chain.ChainStatus[i].Status != X509ChainStatusFlags.RevocationStatusUnknown)
                {
                    chain.ChainPolicy.RevocationFlag = X509RevocationFlag.EntireChain;
                    chain.ChainPolicy.RevocationMode = X509RevocationMode.Online;
                    chain.ChainPolicy.UrlRetrievalTimeout = new TimeSpan(0, 1, 0);
                    chain.ChainPolicy.VerificationFlags = X509VerificationFlags.AllFlags;
                    bool chainIsValid = chain.Build((X509Certificate2)certificate);
                    if (!chainIsValid)
                    {
                        isOk = false;
                    }
                }
            }
        }
        return isOk;
    }
}
