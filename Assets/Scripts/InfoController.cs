﻿using UnityEngine;
using UnityEngine.EventSystems;

public class InfoController : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler {

	public Animator contentAnimator;
	public int infoSetIndex;
	
	public void OnPointerEnter(PointerEventData data){
		contentAnimator.Play("InfoContentAnimationIn");
	}

	public void OnPointerExit(PointerEventData data){
		//HideContent();
	}

	public void HideContent(){
		contentAnimator.Play("InfoContentAnimationOut");
	}

	public void OnClickPlay(){
		MultiMediaController.instance.LoadInfoSet(infoSetIndex);
	}
}
