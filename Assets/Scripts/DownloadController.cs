﻿using UnityEngine;
using UnityEngine.UI;

public class DownloadController : MonoBehaviour {

    public string onlinePath;

    public Button playButton;
    public Button downloadButton;
    public Sprite downloadPauseSprite;
    public Sprite downloadSprite;
    public Transform progressForm;
    public Image progressFill;
    public Text progressText;
    public Text totalText;
    public InputField urlInput;
    public Text pendingRequestsText;
    public string numberofPendingEmailsText;
    public string submittingPendingEmailsText;

    [HideInInspector]
    public State state;
	
    void Awake()
    {
        string prefURL = PlayerPrefs.GetString("VideoURL");
        if (prefURL != null && prefURL != "")
            UpdateOnlineURL(prefURL);
        else
            UpdateOnlineURL(onlinePath);

        Screen.orientation = ScreenOrientation.Portrait;
        AWSHandler.instance.submitPendingEmailsEvent.AddListener(SubmitPendingEmailsEventListener);

        UpdateState();
        OnDownloadComplete(CacheHandler.OnlineUrlToCachePath(onlinePath));
    }

    void OnDestroy()
    {
        AWSHandler.instance.submitPendingEmailsEvent.RemoveListener(SubmitPendingEmailsEventListener);
    }

    void Update()
    {
        if(state == State.Downloading)
        {
            if(!progressForm.gameObject.activeSelf)
                progressForm.gameObject.SetActive(true);

            float progress = CacheHandler.instance.GetDownloadProgress();
            int total = CacheHandler.instance.GetDownloadSize(true);
            progressFill.fillAmount = progress;

            progress = Mathf.Round(progress * 1000f) * 0.1f;    //to percentage
            //progress = Mathf.Round(progress * total * 10f) * 0.1f;    //relative progress

            progressText.text = string.Format("{0:0.0}%", progress);
            totalText.text = string.Format("({0}mb)", total);
        }
    }

    public void OnClickPlay()
    {
        Screen.orientation = ScreenOrientation.Landscape;
        UnityEngine.SceneManagement.SceneManager.LoadScene(1);
    }

    public void OnClickClearCache()
    {
        CacheHandler.instance.ClearCachedFileFromUrl(onlinePath);
        UpdateState();
    }

    public void OnClickDownload()
    {
        if(state == State.Downloading)
        {
            OnClickPauseDownload();
        }
        else
        {
            bool fileExists = CacheHandler.instance.IsOnlineFileCached(onlinePath);
            int fromByte = CacheHandler.instance.CachedOnlineFileSize(onlinePath);

            CacheHandler.instance.DownloadFile(onlinePath, OnDownloadComplete, fromByte);
            //downloadButton.transform.FindChild("Icon").GetComponent<Image>().sprite = downloadPauseSprite;
            //downloadButton.interactable = false;
            urlInput.interactable = false;
            state = State.Downloading;
        }
    }

    public void OnClickPauseDownload()
    {
        CacheHandler.instance.PauseCurrentDownload();
        //downloadButton.transform.FindChild("Icon").GetComponent<Image>().sprite = downloadSprite;
        //downloadButton.interactable = true;
        urlInput.interactable = true;
        state = State.NotCached;
    }

    void OnDownloadComplete(string localPath)
    {
        PlayerPrefs.SetString("VideoPath", localPath);
        UpdateState();
    }

    public void OnURLStringChanged(string inputURL)
    {
        UpdateOnlineURL(inputURL);
    }

    public void SubmitPendingEmailsEventListener(AWSHandler.AWSHandlerState state)
    {
        switch (state)
        {
            case AWSHandler.AWSHandlerState.Ready:
                int pendingEmailCount = AWSEmailPostData.GetCachedEmails().emails.Count;
                if (pendingEmailCount > 0)
                    pendingRequestsText.text = numberofPendingEmailsText + pendingEmailCount;
                else
                    pendingRequestsText.text = "";
                break;

            case AWSHandler.AWSHandlerState.SubmittingNewEmails:
                break;

            case AWSHandler.AWSHandlerState.SubmittingPendingEmails:
                pendingRequestsText.text = submittingPendingEmailsText;
                break;

            default:
                Debug.LogError("Unknown pending email state!");
                break;
        }
    }

    public void UpdateState()
    {
        bool cached = false;

        bool fileExists = CacheHandler.instance.IsOnlineFileCached(onlinePath);
        int fullFileSize = PlayerPrefs.GetInt("DownloadSize", 0);
        int cachedFileSize = CacheHandler.instance.CachedOnlineFileSize(onlinePath);

        if (fileExists && fullFileSize == cachedFileSize)
        {
            cached = true;
        }
        Debug.Log(string.Format("{0} {1} {2}", cached, fullFileSize, cachedFileSize));

        //playButton.gameObject.SetActive(cached);
        //downloadButton.interactable = !cached;
        progressForm.gameObject.SetActive(false);
        urlInput.interactable = true;

        state = cached ? State.Cached : State.NotCached;
    }

    void UpdateOnlineURL(string inputURL)
    {
        PlayerPrefs.SetString("VideoURL", inputURL);
        onlinePath = inputURL;
        urlInput.text = inputURL;
        
        UpdateState();
    }

    public enum State
    {
        NotCached,
        Downloading,
        Cached
    }
}
