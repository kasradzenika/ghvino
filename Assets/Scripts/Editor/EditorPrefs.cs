﻿using UnityEngine;
using UnityEditor;

public class EditorPrefs {
	
	[MenuItem( "Tools/PlayerPrefs.DeleteAll", false)]
	private static void PlayerPrefsDeleteAll(){
        PlayerPrefs.DeleteAll();
    }

}
