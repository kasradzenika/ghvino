﻿using UnityEngine;
using UnityEditor;
using System.Linq;

public class Normals {
	
	[MenuItem( "Tools/Normals/Invert", false)]
	private static void NormalsInvert(){

		for(int i = 0; i < Selection.objects.Length; i++){
			GameObject target = ((GameObject)Selection.objects[i]);

			if(target == null){
				Debug.LogError("Select a GameObjects only!");
				return;
			}

			MeshFilter meshFilter = target.GetComponent<MeshFilter>();

			if(meshFilter == null){
				Debug.LogError("One of the selected objects does not have a MeshFilter component!");
				return;
			}

			//flip mesh inside out
			Mesh mesh = meshFilter.sharedMesh;
			mesh.triangles = mesh.triangles.Reverse().ToArray();
//
//			//flip uv's on X to (or else they appear inverted)
			var uvs = mesh.uv;
			for (int j = 0; j < uvs.Length; j++)
					uvs[j].x = 1.0f - uvs[j].x;

			mesh.uv = uvs;

			//old method
//			Vector3[] normals = mesh.normals;
//
//			for (int n = 0; n < normals.Length; n++) {
//				normals[n].x *= -1f;
//				normals[n].y *= -1f;
//				normals[n].z *= -1f;
//			}
//
//			mesh.normals = normals;
		}

	}

	[MenuItem( "Tools/UVs/Flip Horizontally", false)]
	private static void UVFlipHoriznotally(){
		for(int i = 0; i < Selection.objects.Length; i++){

			Mesh mesh = (Mesh)Selection.objects[i];

			if(mesh == null){
				Debug.LogError("One of the selected objects is not a Mesh!");
				return;
			}
			
			Vector2[] uvs = mesh.uv;
			for (int j = 0; j < uvs.Length; j++)
				uvs[j].x = 1.0f - uvs[j].x;

			mesh.uv = uvs;
		}
	}

	[MenuItem( "Tools/Normals/Set Default", false)]
	private static void SetDefault(){
		
		for(int i = 0; i < Selection.objects.Length; i++){
			GameObject target = ((GameObject)Selection.objects[i]);

			if(target == null){
				Debug.LogError("Select a GameObjects only!");
				return;
			}

			MeshFilter meshFilter = target.GetComponent<MeshFilter>();

			if(meshFilter == null){
				Debug.LogError("One of the selected objects does not have a MeshFilter component!");
				return;
			}

			Mesh mesh = meshFilter.sharedMesh;
			mesh.RecalculateNormals();
		}
	}
}
