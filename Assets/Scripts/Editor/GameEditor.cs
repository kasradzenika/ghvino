﻿using UnityEngine;
using UnityEditor;
using System.Linq;
using System.IO;

public class GameEditor {
	
	[MenuItem( "Tools/Mass Manipulation", false, 61)]
	private static void MassManipulation(){

		for (int i = 0; i < Selection.objects.Length; i++) {
			GameObject t = ((GameObject)Selection.objects[i]);
			t.GetComponent<PinController>().transitionTo = int.Parse(t.name);
			t.name = "Pin";
		}

	}

	[MenuItem( "Tools/Single Manipulation", false, 62)]
	private static void SingleManipulation(){


	}

	[MenuItem("Tools/Prepare Video Files", false, 91)]
	private static void PrepareVideoFiles(){
		string[] files = Directory.GetFiles("./videos");

		for (int i = 0; i < files.Length; i++) {
			File.Move(files[i], files[i].Replace('+', ' '));
		}
	}
}
