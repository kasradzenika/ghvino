﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Camera))]
public class UIController : MonoBehaviour
{
    public GvrReticle reticle;
    public Material reticleMaterial;
    public Color activeColor;
    public Color passiveColor;
    public Transform bottomCanvasPivot;
    public float cameraForwardYThreshold = -0.1f;

    private Quaternion bottomCanvasRotationOffset;
    private bool trackingIsActive = true;

    IEnumerator Start()
    {
        bottomCanvasRotationOffset = Quaternion.FromToRotation(transform.forward, bottomCanvasPivot.forward);

        //gvr reticle needs a moment to set itself up
        yield return new WaitForEndOfFrame();
        ToggleReticle(true);

        while (true)
        {
            yield return new WaitForEndOfFrame();

            if (trackingIsActive)
            {
                if (transform.forward.y < cameraForwardYThreshold)
                {
                    ToggleReticle(true);
                }
                else
                {
                    ToggleReticle(false);
                    FollowCameraRotation();
                }
            }
        }
    }

    public void ToggleReticleTracking(bool toggle)
    {
        reticle.gameObject.SetActive(toggle);
        trackingIsActive = toggle;
    }

    void ToggleReticle(bool toggle)
    {
        reticle.gameObject.SetActive(toggle);
        //reticleMaterial.SetColor("_Color", toggle?activeColor:passiveColor);//not working
    }

    void FollowCameraRotation()
    {
        Quaternion roattion = transform.rotation * bottomCanvasRotationOffset;
        Vector3 eulers = roattion.eulerAngles;
        eulers.z = 0f;
        bottomCanvasPivot.eulerAngles = eulers;
    }
}
