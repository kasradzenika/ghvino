﻿using UnityEngine;
using System.Collections;

public class SFX : MonoBehaviour {

	public AudioClip pinGazeAudio;
	public AudioClip transitionAudio;

	private AudioSource sfxPlayer;

	public static SFX instance;

	void Awake () {
		if(instance == null){
			instance = this;
			DontDestroyOnLoad(gameObject);
		}
		else{
			Destroy(gameObject);
			return;
		}

		sfxPlayer = transform.GetComponent<AudioSource>();
	}
	
	public void PlayPinGazeAudio(){
		//Debug.Log("Play gaze");
		sfxPlayer.clip = pinGazeAudio;
		sfxPlayer.Play();
	}

	public void PausePinGazeAudio(){
		//Debug.Log("Play gaze STOP");
		//sfxPlayer.Stop();
	}

	public void PlayTransitionAudio(){
		//Debug.Log("Play transition");
		sfxPlayer.clip = transitionAudio;
		sfxPlayer.Play();
	}
}
