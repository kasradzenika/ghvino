﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class MainController : MonoBehaviour
{

    public MediaPlayerCtrl media;
    public MultiMediaController multiMediaController;
    public MainStateManager mainStateManager;

    public Text textTakePhoneOut;
    public Text textUI;

    public string introString;
    public string interestedString;

    public Image playPauseImage;
    public Sprite playSprite;
    public Sprite pauseSprite;

    public Image muteImage;
    public Sprite mutedSprite;
    public Sprite unMutedSprite;

    public float touchRotationSensitivity = 2f;
    private bool isVR = false;

    private bool mediaHasVolume = true;

    void Start()
    {
        string videoPath = PlayerPrefs.GetString("VideoPath");
#if UNITY_ANDROID && !UNITY_EDITOR
        videoPath = string.Format("{0}{1}", "file://", videoPath);
#endif
        Debug.Log("Loading video: " + videoPath);

        mainStateManager.State = MainState.Playing;
        Screen.orientation = ScreenOrientation.Landscape;
        isVR = PlayerPrefs.GetInt("IsVRMode") == 1;
    }

    void Update()
    {
        if (!isVR)
            RotateByTouch();

        CheckForClicks();
    }

    public void OnClickMuteUnmute()
    {
        if (mediaHasVolume)
        {
            muteImage.sprite = unMutedSprite;
            media.SetVolume(0f);
        }
        else
        {
            muteImage.sprite = mutedSprite;
            media.SetVolume(1f);
        }

        mediaHasVolume = !mediaHasVolume;
    }

    void SetUpVideoSelectionPanel()
    {
        List<string> options = new List<string>();
        for (int i = 0; i < multiMediaController.videoSets.Length; i++)
        {
            options.Add(multiMediaController.videoSets[i].videoName);
        }
        
    }

    public void OnClickPlayPause()
    {
        if (media.GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.PLAYING)
        {
            playPauseImage.sprite = playSprite;
            media.Pause();
        }
        else if (media.GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.PAUSED ||
                media.GetCurrentState() == MediaPlayerCtrl.MEDIAPLAYER_STATE.STOPPED)
        {
            playPauseImage.sprite = pauseSprite;
            media.Play();
        }
    }

    public void OnClickInterested()
    {
        media.Stop();
        mainStateManager.State = MainState.RemoveHeadset;
    }

    public void OnClickBottomCanvasNoThanks()
    {
        OnClickBack();
    }

    public void OnClickEnterMail()
    {
        mainStateManager.State = MainState.EnteringEmail;
    }

    public void OnClickHome()
    {
        if (multiMediaController.CurrentSet.videoIndex != 0)
        {
            multiMediaController.LoadSet(0);
        }
    }

    public void OnClickBack()
    {
        media.Play();
        mainStateManager.State = MainState.Playing;
    }

    public void OnClickSwitchVR()
    {
        Debug.Log("Switching");
        isVR = !isVR;
        PlayerPrefs.SetInt("IsVRMode", isVR ? 1 : 0);
        //ApplyVRSettings();
    }

    //public void ApplyVRSettings()
    //{
    //    GvrViewer.Instance.VRModeEnabled = isVR;
    //    Camera.main.GetComponent<GvrHead>().enabled = isVR;
    //    vrOffModeVisual.gameObject.SetActive(isVR);
    //    gvrReticle.gameObject.SetActive(isVR);
    //    //standAloneModule.enabled = !isVR; //
    //    gazeModule.enabled = isVR;
    //}

    public float clickInterval = 0.5f;
    private float lastClickTime = -1f;
    private float clickDelta = 0f;
    private int clickCounter = 0;

    void CheckForClicks()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            UnityEngine.SceneManagement.SceneManager.LoadScene(0);
        }

        if (Input.GetMouseButtonDown(0))
        {
            //if not clicking UI stuff
            if (EventSystem.current.currentSelectedGameObject == null)
            {
                clickCounter++;
                lastClickTime = Time.realtimeSinceStartup;
            }

        }

        clickDelta = Time.realtimeSinceStartup - lastClickTime;

        //end clicks
        if (clickCounter > 0 && clickDelta > clickInterval)
        {
            //on single click
            if (clickCounter == 1)
            {
                if (!EventSystem.current.alreadySelecting)
                    OnClickPlayPause();
            }
            else
            {
                //on double or more click
                GvrViewer.Instance.Recenter();
            }

            clickCounter = 0;
        }
    }

    private Vector3 lastMousePos;
    public void RotateByTouch()
    {
        if (Input.GetMouseButtonDown(0))
        {
            lastMousePos = Input.mousePosition;
        }
        //drag
        else if (Input.GetMouseButton(0))
        {
            Vector3 offset = Input.mousePosition - lastMousePos;
            lastMousePos = Input.mousePosition;

            Vector3 rotation = Camera.main.transform.localEulerAngles;
            rotation.x += offset.y * touchRotationSensitivity * Time.deltaTime;
            rotation.y -= offset.x * touchRotationSensitivity * Time.deltaTime;

            Debug.Log(offset);
            Camera.main.transform.localEulerAngles = rotation;
        }
    }
}