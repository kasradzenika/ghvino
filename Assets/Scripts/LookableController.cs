﻿using UnityEngine;
using UnityEngine.Events;
using System.Collections;


public class LookableController : MonoBehaviour {
	
	public float time = 1f;
	private float timer = 0f;

	[HideInInspector]
	public PinState state = PinState.Passive;
	
	public UnityEvent didLookAtEvent;

	void OnEnable(){
		state = PinState.Passive;
	}

	void Update () {
		if(state == PinState.DidStartAction)
			return;

		else if(state == PinState.Active){
			timer += Time.deltaTime;

			if(timer >= time){
				timer = 0f;
				StartAction();
			}
		}
		else{
			timer = 0f;
		}
	}

	void StartAction(){
		didLookAtEvent.Invoke();
		state = PinState.DidStartAction;
	}

	public void OnPointerEnter(){
		state = PinState.Active;
	}

	public void OnPointerExit(){
		state = PinState.Passive;
	}
}
