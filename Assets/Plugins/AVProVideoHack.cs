﻿using UnityEngine;
using System.Collections;

public class AVProVideoHack : MonoBehaviour {

	void Awake () {
#if UNITY_ANDROID && !UNITY_EDITOR
		AndroidJavaClass avHackClass = new AndroidJavaClass("com.VRex.AVProVideoHack.Hack");
		avHackClass.CallStatic("HackAVProVideo");
#endif
	}

}
